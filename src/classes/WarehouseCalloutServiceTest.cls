@IsTest
public class WarehouseCalloutServiceTest
{

    @IsTest
    private static void getEquipmentFromWarehouseTest()
    {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());

        Test.startTest();
            System.enqueueJob(new WarehouseCalloutService());
            WarehouseCalloutService.getEquipment();
        Test.stopTest();

        List<Product2> actualEquipment = [
            SELECT Id
            FROM Product2
            WHERE ExternalId = '100004'];
        System.assertEquals(1, actualEquipment.size());
    }
}