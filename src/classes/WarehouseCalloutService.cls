public with sharing class WarehouseCalloutService implements Queueable
{

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';

    @future (callout = true)
    public static void getEquipment()
    {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(WAREHOUSE_URL);
        request.setMethod('GET');

        HttpResponse httpResponse = http.send(request);
        if (httpResponse.getStatusCode() == 200)
        {
            List<Object> responseData = (List<Object>) JSON.deserializeUntyped(httpResponse.getBody());
            List<Product2> equipmentListForInsertion = new List<Product2>();
            for (Object singleResponseData : responseData)
            {
                Map<String, Object> equipmentNameByValue = (Map<String, Object>) singleResponseData;
                Product2 equipment = new Product2();
                equipment.Replacement_Part__c = (Boolean) equipmentNameByValue.get('replacement');
                equipment.Current_Inventory__c = (Integer) equipmentNameByValue.get('quantity');
                equipment.Name = (String) equipmentNameByValue.get('name');
                equipment.Maintenance_Cycle__c = (Integer) equipmentNameByValue.get('maintenanceperiod');
                equipment.Lifespan_Months__c = (Integer) equipmentNameByValue.get('lifespan');
                equipment.Cost__c = (Decimal) equipmentNameByValue.get('cost');
                equipment.ExternalId = (String) equipmentNameByValue.get('sku');
                equipmentListForInsertion.add(equipment);
            }

            if(responseData.size() == equipmentListForInsertion.size() && !equipmentListForInsertion.isEmpty())
            {
                upsert equipmentListForInsertion;
            }
        }
    }

    public void execute(QueueableContext qCtx)
    {
        getEquipment();
    }
}