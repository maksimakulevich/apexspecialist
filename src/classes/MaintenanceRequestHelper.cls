public with sharing class MaintenanceRequestHelper
{

    @TestVisible private static final String REQUEST_TYPE_REPAIR = 'Repair';
    @TestVisible private static final String REQUEST_TYPE_ROUTINE_MAINTENANCE = 'Routine Maintenance';
    @TestVisible private static final String REQUEST_STATUS_CLOSED = 'Closed';
    @TestVisible private static final String REQUEST_STATUS_NEW = 'New';

    public static void updateWorkOrders(List<Case> newCases)
    {
        List<Case> eligibleClosedMaintenanceRequests = new List<Case>();
        for (Case newCase : newCases)
        {
            if ((newCase.Type == REQUEST_TYPE_REPAIR || newCase.Type == REQUEST_TYPE_ROUTINE_MAINTENANCE) &&
                newCase.Status == REQUEST_STATUS_CLOSED)
            {
                eligibleClosedMaintenanceRequests.add(newCase);
            }
        }

        if (eligibleClosedMaintenanceRequests.isEmpty())
        {
            return;
        }

        List<AggregateResult> minimumCycleDates = [
            SELECT Maintenance_Request__c requestId, MIN(Equipment__r.Maintenance_Cycle__c)
            FROM Equipment_Maintenance_Item__c
            WHERE Maintenance_Request__c IN :eligibleClosedMaintenanceRequests
            GROUP BY Maintenance_Request__c];

        Map<Id, Integer> dueDates = new Map<Id, Integer>();
        for (AggregateResult minimumCycleDate : minimumCycleDates)
        {
            dueDates.put((Id)minimumCycleDate.get('requestId'), Integer.valueOf(minimumCycleDate.get('expr0')));
        }

        List<Case> newMaintenanceRequests = new List<Case>();
        Map<Id, Case> newMaintenanceRequestsById = new Map<Id, Case>();
        for (Case maintenanceRequest : eligibleClosedMaintenanceRequests)
        {
            Case newMaintenanceRequest = new Case();
            newMaintenanceRequest.Vehicle__c = maintenanceRequest.Vehicle__c;
            newMaintenanceRequest.Status = REQUEST_STATUS_NEW;
            newMaintenanceRequest.Type = REQUEST_TYPE_ROUTINE_MAINTENANCE;
            newMaintenanceRequest.Subject = 'Subject';
            newMaintenanceRequest.Date_Reported__c = Date.today();
            if (dueDates.get(maintenanceRequest.Id) != null)
            {
                newMaintenanceRequest.Date_Due__c = Date.today().addDays(dueDates.get(maintenanceRequest.Id));
            }
            else
            {
                newMaintenanceRequest.Date_Due__c = Date.today();
            }
            newMaintenanceRequests.add(newMaintenanceRequest);
            newMaintenanceRequestsById.put(maintenanceRequest.Id, newMaintenanceRequest);
        }

        insert newMaintenanceRequests;

        cloneEquipmentItemsToNewRequests(eligibleClosedMaintenanceRequests, newMaintenanceRequestsById);
    }

    public static void cloneEquipmentItemsToNewRequests(List<Case> eligibleClosedMaintenanceRequests, Map<Id, Case> newMaintenanceRequestsById)
    {
        List<Equipment_Maintenance_Item__c> newEquipmentItems = new List<Equipment_Maintenance_Item__c>();
        List<Equipment_Maintenance_Item__c> oldEquipmentItems = [
            SELECT Equipment__c, Maintenance_Request__c, Quantity__c, Maintenance_Request__r.Id
            FROM Equipment_Maintenance_Item__c
            WHERE Maintenance_Request__c IN :eligibleClosedMaintenanceRequests];
        for (Equipment_Maintenance_Item__c equipmentMaintenanceItem : oldEquipmentItems)
        {
            Equipment_Maintenance_Item__c newEquipmentItem = equipmentMaintenanceItem.clone(false, true, false, false);
            newEquipmentItem.Maintenance_Request__c = newMaintenanceRequestsById.get(equipmentMaintenanceItem.Maintenance_Request__c).Id;
            newEquipmentItems.add(newEquipmentItem);
        }

        insert newEquipmentItems;
    }
}