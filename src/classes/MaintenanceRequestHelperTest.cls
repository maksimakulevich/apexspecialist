@IsTest
public with sharing class MaintenanceRequestHelperTest
{

    @TestSetup
    private static void dataSetup()
    {
        Vehicle__c vehicle = new Vehicle__c();
        vehicle.Name = 'Bike';
        insert vehicle;

        List<Product2> equipments = new List<Product2>();
        Product2 equipment = new Product2();
        equipment.Name = 'Wheel pump';
        equipment.IsActive = true;
        equipment.Maintenance_Cycle__c = 5;
        equipment.Replacement_Part__c = true;
        equipments.add(equipment);

        Product2 nullCycleEquipment = new Product2();
        nullCycleEquipment.Name = 'Nullable Cycle';
        nullCycleEquipment.IsActive = true;
        nullCycleEquipment.Maintenance_Cycle__c = null;
        nullCycleEquipment.Replacement_Part__c = true;
        equipments.add(nullCycleEquipment);

        insert equipments;

        List<Case> maintenanceRequests = new List<Case>();
        for (Integer i = 0; i <= 300; i++)
        {
            Case maintenanceRequest = new Case();
            maintenanceRequest.Subject = 'Subject';
            maintenanceRequest.Type = 'Repair';
            maintenanceRequest.Status = MaintenanceRequestHelper.REQUEST_STATUS_NEW;
            maintenanceRequest.Origin = 'Web';
            if (i < 50)
            {
                maintenanceRequest.ProductId = nullCycleEquipment.Id;
            }
            else
            {
                maintenanceRequest.ProductId = equipment.Id;
            }
            maintenanceRequest.Vehicle__c = vehicle.Id;
            maintenanceRequests.add(maintenanceRequest);
        }
        insert maintenanceRequests;

        List<Equipment_Maintenance_Item__c> equipmentMaintenanceItems = new List<Equipment_Maintenance_Item__c>();
        for (Integer i = 0; i <= 300; i++)
        {
            Equipment_Maintenance_Item__c equipmentMaintenanceItem = new Equipment_Maintenance_Item__c();
            if (i < 50)
            {
                equipmentMaintenanceItem.Equipment__c = nullCycleEquipment.Id;
            }
            else
            {
                equipmentMaintenanceItem.Equipment__c = equipment.Id;
            }
            equipmentMaintenanceItem.Maintenance_Request__c = maintenanceRequests.get(i).Id;
            equipmentMaintenanceItems.add(equipmentMaintenanceItem);
        }
        insert equipmentMaintenanceItems;
    }

    @IsTest
    private static void updateWorkOrdersPositiveTest()
    {
        List<Case> maintenanceRequests = [
            SELECT Id, Status, ProductId
            FROM Case
            WHERE Status = :MaintenanceRequestHelper.REQUEST_STATUS_NEW
            LIMIT 1];

        maintenanceRequests[0].Status = MaintenanceRequestHelper.REQUEST_STATUS_CLOSED;

        Test.startTest();
            update maintenanceRequests;
        Test.stopTest();

        List<Case> actualMaintenanceRequests = [
            SELECT Id, Type, Status
            FROM Case
            WHERE Status = :MaintenanceRequestHelper.REQUEST_STATUS_NEW
                AND Type = :MaintenanceRequestHelper.REQUEST_TYPE_ROUTINE_MAINTENANCE];

        System.assertEquals(1, actualMaintenanceRequests.size());
    }

    @IsTest
    private static void updateWorkOrdersNegativeTest()
    {
        List<Case> maintenanceRequests = [
            SELECT Id, Status
            FROM Case
            WHERE Status = :MaintenanceRequestHelper.REQUEST_STATUS_NEW];

        maintenanceRequests[0].Status = 'Working';

        Test.startTest();
            update maintenanceRequests;
        Test.stopTest();

        List<Case> actualMaintenanceRequests = [
            SELECT Id, Type, Status
            FROM Case
            WHERE Status = :MaintenanceRequestHelper.REQUEST_STATUS_NEW];
        System.assertEquals(maintenanceRequests.size()-1, actualMaintenanceRequests.size());
    }

    @IsTest
    private static void updateWorkOrdersBulkTest()
    {
        List<Case> maintenanceRequests = [
            SELECT Id, Status
            FROM Case
            LIMIT 300];

        for (Case maintenanceRequest : maintenanceRequests)
        {
            maintenanceRequest.Status = 'Closed';
        }

        Test.startTest();
            update maintenanceRequests;
        Test.stopTest();

        List<Case> actualMaintenanceRequests = [
            SELECT Id, Type, Status
            FROM Case
            WHERE Status = :MaintenanceRequestHelper.REQUEST_STATUS_NEW
                AND Type = :MaintenanceRequestHelper.REQUEST_TYPE_ROUTINE_MAINTENANCE];

        System.assertEquals(300, actualMaintenanceRequests.size());
    }
}