global with sharing class WarehouseSyncSchedule implements Schedulable
{
    global void execute(SchedulableContext sCtx)
    {
        System.enqueueJob(new WarehouseCalloutService());
    }
}