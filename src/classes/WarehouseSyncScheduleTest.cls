@IsTest
public class WarehouseSyncScheduleTest
{

    private static final String CRON_EXP = '0 0 * * * ?';

    @IsTest
    private static void executeWarehouseScheduleJobTest()
    {
        Test.startTest();
            String jobId = System.schedule('WarehouseSyncScheduleTestJob', CRON_EXP, new WarehouseSyncSchedule());
        Test.stopTest();

        List<AsyncApexJob> actualScheduledJob = [
            SELECT Id, ApexClassId, ApexClass.Name, Status, JobType
            FROM AsyncApexJob
            WHERE Id =: jobId
                AND JobType = 'ScheduledApex'];

        System.assertNotEquals(1, actualScheduledJob.size());
    }
}